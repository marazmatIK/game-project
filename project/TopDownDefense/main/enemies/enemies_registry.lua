ENEMIES_REGISTRY_OBJ = {}

ENEMIES_REGISTRY_OBJ.enemies = {}
ENEMIES_REGISTRY_OBJ.reset = function() 
	ENEMIES_REGISTRY_OBJ.enemies = {}
end

ENEMIES_REGISTRY_OBJ.register = function(id) 
	ENEMIES_REGISTRY_OBJ.enemies[id] = id
end

ENEMIES_REGISTRY_OBJ.unregister = function(id) 
	ENEMIES_REGISTRY_OBJ.enemies[id] = nil
end

ENEMIES_REGISTRY_OBJ.get_all = function() 
	return ENEMIES_REGISTRY_OBJ.enemies
end

ENEMIES_REGISTRY_OBJ.debug_print_all = function() 
	for k, v in pairs(ENEMIES_REGISTRY_OBJ.enemies) do
		print(v)
	end
end

return ENEMIES_REGISTRY_OBJ