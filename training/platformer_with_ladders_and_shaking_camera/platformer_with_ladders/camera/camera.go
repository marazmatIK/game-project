components {
  id: "camera"
  component: "/platformer_with_ladders/camera/screen_shake.camera"
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
components {
  id: "hammer_shake"
  component: "/platformer_with_ladders/scripts/hammer_shake.script"
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
